package me.jax.codertrial;



import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class CoderTrial extends JavaPlugin implements Listener{
	
	ArrayList<UUID> tramplers = new ArrayList<UUID>();
	
    @Override
    public void onEnable() {
        Bukkit.getServer().getPluginManager().registerEvents(this, this);
        List<String> temp = getConfig().getStringList("path");
        tramplers = (ArrayList<UUID>) temp.parallelStream().map(UUID::fromString).collect(Collectors.toList());
        
    }
    
    @Override
    public void onDisable() {
    	List<String> serialized = tramplers.parallelStream().map(UUID::toString).collect(Collectors.toList());
    	getConfig().set("path", serialized);
    	saveConfig();
    }
    
    enum cropType {
    	WHEAT(Material.CROPS, Material.WHEAT, Material.SEEDS),
    	POTATO(Material.POTATO, Material.POTATO_ITEM, null),
    	CARROT(Material.CARROT, Material.CARROT_ITEM, null),
    	BEETROOT(Material.BEETROOT_BLOCK, Material.BEETROOT, Material.BEETROOT_SEEDS),
    	NETHERWART(Material.NETHER_WARTS, Material.NETHER_WARTS, null);
    	
    	private Material drops;
    	private Material seeds;
    	private Material type;
    	private cropType(Material blockType, Material drops, Material seeds) {
    		this.drops = drops;
    		this.seeds = seeds;
    		this.type = blockType;
    	}
    	
    	public Material getDrops() {
			return drops;
    	}
    	public Material getSeeds() {
    		return seeds;
    	}
    	public Material getType() {
    		return type;
    	}

    	public void setInfo(Block b, Player p) {
    		for (cropType c : cropType.values()) { //loops through cropTypes
    			   if (c.getType() == b.getType()) { //if cropType = block broken
    				   this.drops = c.getDrops(); //sets
    				   this.seeds = c.getSeeds();
    				   this.type = c.getType();
    			   }
    			}
    	}
    	
    	
    }
    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Player p = (Player) e.getPlayer();
        Block b = (Block) e.getBlock();
        ItemStack i = p.getInventory().getItemInMainHand();
        Tools tools = new Tools();
        
        cropType crops = cropType.NETHERWART; //initializes
		
        if (tools.isValidCrop(b)){ //checks if block is a grown crop
        	crops.setInfo(b, p);
        	ItemStack d = new ItemStack(crops.getDrops(), tools.getDropAmount(i));
        	e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), d);//drops crop
        	if (crops.getSeeds() != null) { //if there is a seed drop
        		d = new ItemStack(crops.getSeeds(), tools.getDropAmount(i));
        	e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), d);//drops seeds
        	}
        	
        	
        	i.setDurability((short) (i.getDurability() + tools.getDuraDamage(i))); //does durability damage
        	if (i.getDurability() >= i.getType().getMaxDurability()) { // if item @ max durability
        		p.getInventory().setItemInMainHand(null); //removes item
        		p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1F, 1F); //plays sound of item breaking
        	}
        	
        	int rnd = (int)(Math.random()*300); //random events
        	if (rnd == 1) {
        		p.sendMessage(ChatColor.GREEN + "You really ought to be careful with your pesticides!");
        		p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20, 5));
        	} else if (rnd == 2) {
        		p.sendMessage(ChatColor.GOLD + "Lucky you! Double yield!");
        		e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), d);
        	} else if (rnd == 3) {
        		p.sendMessage(ChatColor.DARK_GREEN + "Generally farming in a graveyard is a bad idea.");
        		b.getWorld().spawnEntity(p.getLocation(), EntityType.ZOMBIE);
        	}
        	e.getBlock().getWorld().getBlockAt(e.getBlock().getLocation()).setType(Material.AIR); //removes block after all is done
        	
        	//following two lines allows for hawkeye to log the event
        	BlockBreakEvent fakeE = new BlockBreakEvent(b, p.getPlayer());
        	Bukkit.getServer().getPluginManager().callEvent(fakeE);
        }
        	
    	}
        	
        	
	//}
    
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	if (cmd.getName().equalsIgnoreCase("trample")) {
    		if (sender instanceof Player) {
    			if (args.length < 1) {
    			UUID p = ((Player) sender).getUniqueId();
    			boolean found = false;
				for (int count = 0; count < tramplers.size(); count++) { //traverses list of trampler enabled players
					if (tramplers.get(count).equals(p)) { //if player is on the list
						found = true;
						tramplers.remove(count); //remove player from list
						sender.sendMessage(Bukkit.getPlayer(p).getDisplayName() + "'s trampling has been disabled.");
						count--; //iterator - 1
					}
				}
				if (found == false) { //if player not on list
					tramplers.add(Bukkit.getPlayer(p).getUniqueId()); //add player to list
					sender.sendMessage(Bukkit.getPlayer(p).getDisplayName() + "'s trampling has been enabled.");
				}
    			} else if (args.length == 1) {
    				if (Bukkit.getServer().getPlayer(args[0]) != null) { //makes sure player is online
    				UUID p = Bukkit.getServer().getPlayer(args[0]).getUniqueId(); //finds player	
    				if (Bukkit.getPlayer(p).hasPermission("codertrial.tramplemod")) { //checks if player has permission to execute tramplemod
    					boolean found = false;
    					for (int count = 0; count < tramplers.size(); count++) { //traverses list of trampler enabled players
    						if (tramplers.get(count).equals(p)) { //if player is on the list
    							found = true;
    							tramplers.remove(count); //remove player from list
    							sender.sendMessage(Bukkit.getPlayer(p).getDisplayName() + "'s trampling has been disabled.");
    							count--; //iterator - 1
    						}
    					}
    					if (found == false) { //if player not on list
    						tramplers.add(p); //add player to list
    						sender.sendMessage(Bukkit.getPlayer(p).getDisplayName() + "'s trampling has been enabled.");
    					}
    				} else {
    					sender.sendMessage(ChatColor.RED + "You do not have permission to perform this command.");
    				}
    				} else {
    					sender.sendMessage(ChatColor.RED + "Error: Player is not online.");
    				}
    			} else {
    				sender.sendMessage("Correct usage: /trample [user/blank]");
    			}
    		} else {
    			sender.sendMessage("You must be a player to execute this command.");
    		
    		}
    		
    	}
    		
		return true;
    }
    
    @EventHandler
    public void trampleCheck(PlayerInteractEvent e)
    {
        if(e.getAction()== Action.PHYSICAL && e.getClickedBlock().getType() == Material.SOIL) {
        	boolean found = false;
        	for (int count = 0; count < tramplers.size(); count++) {
        		if (Bukkit.getPlayer(tramplers.get(count)).equals(e.getPlayer())) {
        			found = true;
        		}
        	}
        	if (found == false) {
        		e.setCancelled(true);  	
        	}
    }
    
}
}